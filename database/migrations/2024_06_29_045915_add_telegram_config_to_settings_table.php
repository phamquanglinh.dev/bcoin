<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddTelegramConfigToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::table('settings')->updateOrInsert([
           'name' => 'telegram_chat_id',
       ],[
           'name' => 'telegram_chat_id',
           'value' => '-4233804133',
       ]);

        DB::table('settings')->updateOrInsert([
            'name' => 'telegram_bot_token',
        ],[
            'name' => 'telegram_bot_token',
            'value' => '7333457843:AAHBcGgS45ZKzw6OaVtoLuF5c8MkQtrBkyA',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            //
        });
    }
}
