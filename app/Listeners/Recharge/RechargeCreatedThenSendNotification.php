<?php

namespace App\Listeners\Recharge;

use App\Events\Recharge\RechargeCreated;
use App\Jobs\SendRechargeCreatedNotification;
use Illuminate\Contracts\Bus\QueueingDispatcher;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RechargeCreatedThenSendNotification
{
    private QueueingDispatcher $queueingDispatcher;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        QueueingDispatcher $queueingDispatcher
    )
    {
        //
        $this->queueingDispatcher = $queueingDispatcher;
    }

    /**
     * Handle the event.
     *
     * @param RechargeCreated $event
     * @return void
     */
    public function handle(RechargeCreated $event)
    {
        $recharge = $event->getRecharge();

        $this->queueingDispatcher->dispatch(new SendRechargeCreatedNotification($recharge));
    }
}
