<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    const IN = 0;
    const OUT = 1;
    protected $table = 'transactions';

    protected $guarded = ['id'];

    protected $visible = [
        'id',
        'user_id',
        'wallet_id',
        'amount',
        'title',
        'description',
        'action_type',
        'created_at',
    ];
}
