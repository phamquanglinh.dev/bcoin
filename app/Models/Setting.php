<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $visible = ['id', 'name', 'value'];

    public static function getMappedSettings(): array
    {
        return Setting::query()->get()->mapWithKeys(function ($setting) {
            return [$setting['name'] => $setting['value']];
        })->toArray();
    }
}
