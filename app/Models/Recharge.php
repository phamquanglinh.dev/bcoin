<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Recharge extends Model
{
    use HasFactory;

    const STATUS_WAITING = 0;
    const STATUS_PAID = 1;
    const STATUS_CANCELLED = 2;

    protected $table = 'recharges';
    protected $guarded = ['id'];

    protected $visible = [
        'id',
        'user_id',
        'wallet_id',
        'user',
        'wallet',
        'amount',
        'status',
        'status_label'
    ];

    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function Wallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class, 'wallet_id', 'id');
    }

    public function getStatusLabelAttribute(): string
    {
        return $this->mappedStatusLabel()[$this->{'status'}];
    }

    private function mappedStatusLabel(): array
    {
        return [
            self::STATUS_WAITING => 'waiting',
            self::STATUS_PAID => 'paid',
            self::STATUS_CANCELLED => 'cancel',
        ];
    }
}
