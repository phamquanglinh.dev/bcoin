<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    use HasFactory;

    protected $table = 'wallets';
    protected $guarded = ['id'];

    protected $visible = [
        'id',
        'user_id',
        'public_key',
        'private_key',
        'balance'
    ];
}
