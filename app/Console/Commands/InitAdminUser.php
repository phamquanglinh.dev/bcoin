<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class InitAdminUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init_admin_user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = User::query()->updateOrCreate([
            'username' => 'admin'
        ], [
            'name' => 'admin',
            'username' => 'admin',
            'public_key' => Str::random(32),
            'password' => Hash::make("Linhz123@"),
        ]);

        $wallet = Wallet::query()->updateOrCreate([
            'user_id' => $user->getAttribute('id'),
        ], [
            'user_id' => $user->getAttribute('id'),
            'balance' => 1000000,
            'public_key' => $user['public_key'],
            'private_key' => Str::random(32),
        ]);

        return 1;
    }
}
