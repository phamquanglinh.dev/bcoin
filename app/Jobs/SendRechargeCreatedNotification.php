<?php

namespace App\Jobs;

use App\Models\Recharge;
use App\Models\Setting;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendRechargeCreatedNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Recharge $recharge;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        Recharge $recharge
    )
    {
        //
        $this->recharge = $recharge;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GuzzleException
     */
    public function handle(): void
    {
        $settings = Setting::getMappedSettings();

        $telegramChatId = $settings['telegram_chat_id'];

        $telegramBotToken = $settings['telegram_bot_token'];

        $this->sendMessageWithButtons($telegramChatId, $telegramBotToken);
    }

    /**
     * @param $telegramChatId
     * @param $telegramBotToken
     * @return string
     * @throws GuzzleException
     */
    public function sendMessageWithButtons($telegramChatId, $telegramBotToken): string
    {
        $recharge = $this->recharge;

        $client = new Client();
        $url = "https://api.telegram.org/bot{$telegramBotToken}/sendMessage";

        $user = User::query()->where('id', $recharge['user_id'])->first();

        $amount = number_format($recharge['amount']);
        $rechargeCode = $recharge['type'];

        $message = "Đơn nạp tiền mới:\n";
        $message .= "Tên: {$user['name']}\n";
        $message .= "Username: {$user['username']}\n";
        $message .= "Public_key: {$user['public_key']}\n";
        $message .= "Số tiền: $amount\n";
        $message .= "RechargeCode: $rechargeCode\n";

        $response = $client->post($url, [
            'json' => [
                'chat_id' => $telegramChatId,
                'text' => $message,
                'reply_markup' => [
                    'inline_keyboard' => [
                        [
                            ['text' => 'Duyệt', 'callback_data' => 'approve_' . $recharge['id']],
//                            ['text' => 'Từ chối', 'callback_data' => 'reject_' . $recharge['id']]
                        ]
                    ]
                ]
            ]
        ]);

        return $response->getBody()->getContents();
    }
}
