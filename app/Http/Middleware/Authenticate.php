<?php

namespace App\Http\Middleware;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param Request $request
     * @return string|null
     * @throws AuthenticationException
     */
    protected function redirectTo($request): ?string
    {
        Log::debug($request->headers);
        if (!$request->expectsJson()) {
            throw new AuthenticationException();
        }

        return next($request);
    }
}
