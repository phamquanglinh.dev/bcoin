<?php

namespace App\Http\Controllers;

use App\Events\User\GetAllUser;
use App\Exceptions\ForbiddenException;
use App\Models\Setting;
use App\Models\User;
use App\Models\Wallet;
use App\Services\ResourceAbilityService;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class AuthenticateController extends Controller
{
    /**
     * @throws ValidationException
     * @throws AuthenticationException
     */
    public function loginAction(Request $request): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'username' => 'required|exists:users,username|string',
            'password' => 'required|string'
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification);
        }

        $user = User::query()->where('username', $input['username'])->first();

        if (! $user) {
            throw new AuthenticationException();
        }

        if (!Hash::check($input['password'], $user->password)) {
            throw new AuthenticationException();
        }

        $token = $user->createToken('Bearer')->plainTextToken;

        return response()->json([
            'token' => $token
        ]);
    }

    /**
     * @param Request $request
     * @param HttpResponse $httpResponse
     * @param ResourceAbilityService $resourceAbilityService
     * @return JsonResponse
     */
    public function getUserInformation(Request $request, HttpResponse $httpResponse, ResourceAbilityService $resourceAbilityService): JsonResponse
    {
        Log::debug($request->headers);
        /**
         * @var User $user
         */
        $user = $request->user()->load(['wallet']);

        event(new GetAllUser($user));

        return $httpResponse->responseModelData($user);
    }

    /**
     * @throws ValidationException
     */
    public function registerUser(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $validator = Validator::make($input, [
            'username' => 'required|string|unique:users,username',
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required|string',
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $user = new User();

        $user->fill($input);

        $user->save();

        return $httpResponse->responseCreated($user['id']);
    }

    /**
     * @param Request $request
     * @param ResourceAbilityService $resourceAbilityService
     * @param HttpResponse $httpResponse
     * @return JsonResponse
     * @throws ForbiddenException
     */
    public function getAllUsersAction(Request $request, ResourceAbilityService $resourceAbilityService, HttpResponse $httpResponse): JsonResponse
    {
        $user = $request->user();

        if (!$resourceAbilityService->canUserAccessSetting($user)) {
            throw new ForbiddenException();
        }

        $builder = User::query();

        $this->handleQuery($builder, $request);

        return $httpResponse->responseDataCollection($builder->get());
    }

    /**
     * @throws ForbiddenException
     */
    public function approveUserAction(Request $request, HttpResponse $httpResponse, ResourceAbilityService $resourceAbilityService, $userId): JsonResponse
    {
        $interactive = $request->user();

        $settings = Setting::getMappedSettings();

        $genesisAmount = $settings['default_wallet_amount'] ?? 0;

        if (!$resourceAbilityService->canUserAccessSetting($interactive)) {
            throw new ForbiddenException();
        }

        $user = User::query()->find($userId);

        if (!$user) {
            throw new ModelNotFoundException();
        }

        $publicKey = Str::random(32);

        $user->setAttribute('public_key', $publicKey);

        $wallet = new Wallet();

        $wallet->fill([
            'user_id' => $user['id'],
            'public_key' => $publicKey,
            'private_key' => Str::random(32),
            'balance' => $genesisAmount,
        ]);

        $wallet->save();

        return $httpResponse->responseCreated($wallet['id']);
    }
}
