<?php

namespace App\Http\Controllers;

use App\Events\Recharge\RechargeCreated;
use App\Exceptions\ForbiddenException;
use App\Models\Recharge;
use App\Models\Setting;
use App\Services\ResourceAbilityService;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RechargeController extends Controller
{
    /**
     * @throws ValidationException
     * @throws ForbiddenException
     */
    public function createRechargeAction(Request $request, HttpResponse $httpResponse, ResourceAbilityService $resourceAbilityService): JsonResponse
    {
        $user = $request->user();

        if (!$resourceAbilityService->canUserCreateRecharge($user)) {
            throw new ForbiddenException();
        }

        $input = $request->input();

        $notification = Validator::make($input, [
            'amount' => 'required|numeric',
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification);
        }

        $settings = Setting::getMappedSettings();

        $client = new Client();

        $url = 'https://api.vietqr.io/v2/generate';

        $headers = [
            'x-client-id' => config('viet_qr_client_id'),
            'x-api-key' => config('viet_qr_api_key'),
        ];

        $rechargeCode = Str::random(9);

        $body = [
            'accountNo' => $settings['default_bank_account'],
            'accountName' => $settings['default_bank_name'],
            'acqId' => $settings['default_bank_code'],
            'amount' => $input['amount'],
            'addInfo' => "BCOIN {$rechargeCode} {$user->username} {$input['amount']}",
            'format' => 'text',
            "template" => 'print'
        ];

        $response = $this->tryApi(
            fn() => $client->post($url, [
                'headers' => $headers,
                'json' => $body
            ])
        );

        $responseData = json_decode($response->getBody()->getContents(), true);

        $recharge = new Recharge();

        $recharge->fill([
            'wallet_id' => $user->wallet?->id,
            'amount' => $input['amount'],
            'user_id' => $user->id,
            'status' => Recharge::STATUS_WAITING,
            'type' => $rechargeCode,
        ]);

        $recharge->save();

        event(new RechargeCreated($recharge));

        return $httpResponse->responseData($responseData);
    }

    /**
     * @throws ForbiddenException
     */
    public function previewRechargeAction(Request $request, HttpResponse $httpResponse, ResourceAbilityService $resourceAbilityService, $id): JsonResponse
    {
        $user = $request->user();

        /**
         * @var Recharge $recharge
         */
        $recharge = Recharge::query()->where('id', $id)->first();

        if (!$recharge) {
            throw new ModelNotFoundException();
        }

        if (!$resourceAbilityService->canUserAccessRecharge($recharge, $user)) {
            throw new ForbiddenException();
        }

        $settings = Setting::getMappedSettings();

        $client = new Client();

        $url = 'https://api.vietqr.io/v2/generate';

        $headers = [
            'x-client-id' => config('viet_qr_client_id'),
            'x-api-key' => config('viet_qr_api_key'),
        ];

        $body = [
            'accountNo' => $settings['default_bank_account'],
            'accountName' => $settings['default_bank_name'],
            'acqId' => $settings['default_bank_code'],
            'amount' => $recharge['amount'],
            'addInfo' => "BCOIN {$recharge['type']} {$user->username} {$recharge['amount']}",
            'format' => 'text',
            "template" => 'print'
        ];

        $response = $this->tryApi(
            fn() => $client->post($url, [
                'headers' => $headers,
                'json' => $body
            ])
        );

        $responseData = json_decode($response->getBody()->getContents(), true);

        return $httpResponse->responseData($responseData);
    }

    public function getAllRechargeAction(Request $request, HttpResponse $httpResponse, ResourceAbilityService $resourceAbilityService): JsonResponse
    {
        $user = $request->user();

        $builder = Recharge::query();

        if (!$resourceAbilityService->canUserAccessSetting($user)) {
            $builder->where('user_id', $user->id);
        }

        $this->handleQuery($builder, $request);

        $recharges = $builder->get()->load(['user','wallet'])->append(['status_label']);

        return $httpResponse->responseData($recharges);
    }
}
