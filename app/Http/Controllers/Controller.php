<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function tryApi(\Closure $callback): ResponseInterface
    {
        try {
            return $callback();
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }
    }

    public function handleQuery(Builder $builder, Request $request): void
    {
        $conditions = $request->get('filters', []);
        $limit = $request->get('limit', 20);
        $page = $request->get('page', 1);
        $sort = $request->get('sort', 'created_at');
        $direction = $request->get('direction', 'desc');

        foreach ($conditions as $conditionKey => $value) {
            $column = explode(':', $conditionKey)[0];

            $operator = $this->mappedOperator()[explode(':', $conditionKey)[1] ?? 'eq'];

            if ($operator == 'like' || $operator == 'not like') {
                $value = "%{$value}%";
            }

            $builder->where($column, $operator, $value);
        }

        $builder->orderBy($sort, $direction)->limit($limit)->offset(($page - 1) * $limit);
    }

    private function mappedOperator(): array
    {
        return [
            'eq' => '=',
            'contain' => 'like',
            'ne' => 'not like',
            'gte' => '>=',
            'lte' => '<=',
            'gt' => '>',
            'lt' => '<',
            'in' => 'in'
        ];
    }
}
