<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;

class HttpResponse
{
    public function responseSuccess($data = []): JsonResponse
    {
        return response()->json($data, 200);
    }

    public function responseMessage(string $message): JsonResponse
    {
        return response()->json([
            'message' => $message
        ], 200);
    }

    public function responseCreated($identity): JsonResponse
    {
        return response()->json([
            'message' => 'Successfully created',
            'id' => $identity
        ], 201);
    }

    public function responseUpdated(): JsonResponse
    {
        return response()->json([
            'message' => 'Successfully updated',
        ]);
    }

    public function responseDeleted(): JsonResponse
    {
        return response()->json([
            'message' => 'Successfully deleted',
        ]);
    }

    public function responseUnExpectedError(): JsonResponse
    {
        return response()->json([
            'message' => 'Unexpected error',
        ]);
    }

    public function responseModelData(Model $model): JsonResponse
    {
        return response()->json($model->toArray());
    }

    public function responseData($data): JsonResponse
    {
        return response()->json($data);
    }

    public function responseDataCollection(Collection $collection): JsonResponse
    {
        return response()->json($collection->toArray());
    }
}
