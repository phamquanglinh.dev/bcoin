<?php

namespace App\Http\Controllers;

use App\Models\Recharge;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TelegramController extends Controller
{
    /**
     * @throws GuzzleException
     */
    public function handleWebhook(Request $request): string
    {
        $telegramToken = Setting::getMappedSettings()['telegram_bot_token'];

        $update = $request->all();

        if (isset($update['callback_query'])) {
            $callbackQuery = $update['callback_query'];

            $data = $callbackQuery['data'];

            $chatId = $callbackQuery['message']['chat']['id'];

            $messageId = $callbackQuery['message']['message_id'];

            [$action, $id] = explode('_', $data);

            /**
             * @var Recharge $recharge
             */
            $recharge = Recharge::query()->where('id', $id)->first();

            if (!$recharge) {
                return 'not_found';
            }

            $this->apporveRecharge($recharge);

            $client = new Client();

            $url = "https://api.telegram.org/bot{$telegramToken}/editMessageText";

            $response = $client->post($url, [
                'json' => [
                    'chat_id' => $chatId,
                    'message_id' => $messageId,
                    'text' => "Đã duyệt giao dịch {$recharge['type']}"
                ]
            ]);

            return $response->getBody()->getContents();
        }

        return 'OK';
    }

    private function apporveRecharge(Recharge $recharge): void
    {
        $recharge->setAttribute('status', Recharge::STATUS_PAID);

        $transaction = new Transaction();

        /**
         * @var Wallet $wallet
         */
        $wallet = Wallet::query()->where('user_id', $recharge['user_id'])->first();

        if (!$wallet) {
            return;
        }

        $transaction->fill([
            'user_id' => $wallet['user_id'],
            'amount' => $recharge['amount'],
            'title' => 'Quà tặng từ BSM',
            'description' => "Nạp Bcoin Từ giao dịch {$recharge['type']}",
            'action_type' => 0,
            'direction' => Transaction::IN,
            'wallet_id' => $wallet['id'],
        ]);

        $transaction->save();

        $wallet->setAttribute('balance', $wallet['balance'] - $recharge['amount']);

        $wallet->save();
    }
}
