<?php

namespace App\Http\Controllers;

use App\Exceptions\ForbiddenException;
use App\Models\Setting;
use App\Services\ResourceAbilityService;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * @param Request $request
     * @param ResourceAbilityService $resourceAbilityService
     * @param HttpResponse $httpResponse
     * @return JsonResponse
     * @throws ForbiddenException
     */
    public function getAllBankAction(Request $request, ResourceAbilityService $resourceAbilityService, HttpResponse $httpResponse)
    {
        $user = $request->user();

        if (!$resourceAbilityService->canUserAccessSetting($user)) {
            throw new ForbiddenException();
        }

        $url = 'https://api.vietqr.io/v2/banks';

        $client = new Client();

        $response = $this->tryApi(
            fn() => $client->get($url)
        );

        $responseData = json_decode($response->getBody()->getContents(), true);

        return $httpResponse->responseData(array_filter($responseData['data']));
    }

    /**
     * @param Request $request
     * @param ResourceAbilityService $resourceAbilityService
     * @param HttpResponse $httpResponse
     * @return JsonResponse
     * @throws ForbiddenException
     */
    public function getInitSettingValueAction(Request $request, ResourceAbilityService $resourceAbilityService, HttpResponse $httpResponse): JsonResponse
    {
        $user = $request->user();

        if (!$resourceAbilityService->canUserAccessSetting($user)) {
            throw new ForbiddenException();
        }

        $settings = Setting::query()->get();

        return $httpResponse->responseDataCollection($settings);
    }
}
