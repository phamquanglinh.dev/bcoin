<?php

namespace App\Http\Controllers;

use App\Exceptions\ForbiddenException;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Services\ResourceAbilityService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class TransactionController extends Controller
{
    /**
     * @throws ForbiddenException
     * @throws ValidationException
     */
    public function createTransactionAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $user = $request->user();

        $input = $request->input();

        $notification = Validator::make($input, [
            'private_key' => 'required|string',
            'public_key' => 'required|string',
            'amount' => 'required|numeric',
            'action_type' => 'required|integer',
            'title' => 'string|required',
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification);
        }

        $user = User::query()->where([
            'public_key' => $input['public_key'],
            'id' => $user->id,
        ])->first();

        $wallet = Wallet::query()->where([
            'private_key' => $input['private_key'],
            'public_key' => $input['public_key'],
        ])->first();

        if ((!$user) || (!$wallet)) {
            throw new ForbiddenException();
        }

        $transaction = new Transaction();

        $transaction->fill([
            'user_id' => $user->id,
            'amount' => $input['amount'],
            'title' => $input['title'],
            'description' => $input['description'] ?? '',
            'wallet_id' => $wallet->id,
            'direction' => Transaction::OUT,
            'action_type' => $input['action_type'],
        ]);

        $transaction->save();

        $wallet->balance -= $input['amount'];

        $wallet->save();

        return $httpResponse->responseCreated($transaction['id']);
    }

    public function getAllTransactionAction(Request $request, HttpResponse $httpResponse, ResourceAbilityService $resourceAbilityService): JsonResponse
    {
        $user = $request->user();

        $builder = Transaction::query();

        #WRAP BUILDER
        if (!$resourceAbilityService->canUserAccessSetting($user)) {
            $builder->where('user_id', $user->id);
        }

        $this->handleQuery($builder, $request);

        $transactions = $builder->get();

        return $httpResponse->responseSuccess($transactions);
    }

    /**
     * @throws ForbiddenException
     * @throws ValidationException
     */
    public function createGiftTransactionAction(Request $request, HttpResponse $httpResponse, ResourceAbilityService $resourceAbilityService): JsonResponse
    {
        $interactive = $request->user();

        if (!$resourceAbilityService->canUserAccessSetting($interactive)) {
            throw new ForbiddenException();
        }

        $input = $request->input();

        $notification = Validator::make($input, [
            'public_key' => 'required|string',
            'amount' => 'required|numeric',
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification);
        }

        $wallet = Wallet::query()->where([
            'public_key' => $input['public_key'],
        ])->first();

        if (!$wallet) {
            throw new ModelNotFoundException();
        }

        $transaction = new Transaction();

        $numberedAmount = $input['amount'];

        $transaction->fill([
            'user_id' => $wallet['user_id'],
            'amount' => $input['amount'],
            'title' => 'Quà tặng từ BSM',
            'description' => "Tặng bạn {$numberedAmount} đ trải nghiệm",
            'action_type' => 0,
            'direction' => Transaction::IN,
            'wallet_id' => $wallet['id'],
        ]);

        $transaction->save();


    }
}
