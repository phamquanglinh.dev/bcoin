<?php

namespace App\Events\Recharge;

use App\Models\Recharge;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RechargeCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private Recharge $recharge;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        Recharge $recharge
    )
    {
        $this->recharge = $recharge;
    }

    public function getRecharge(): Recharge
    {
        return $this->recharge;
    }
}
