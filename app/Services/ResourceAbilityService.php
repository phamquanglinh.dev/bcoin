<?php

namespace App\Services;

use App\Models\Recharge;
use App\Models\User;

class ResourceAbilityService
{
    public function canUserAccessSetting(User $user): bool
    {
        return $user->getAttribute('username') == 'admin';
    }

    public function canUserCreateRecharge(User $user): bool
    {
        return true;
    }

    public function canUserAccessRecharge(Recharge $recharge, User $user): bool
    {
        return $recharge->getAttribute('user_id') == $user->getAttribute('id');
    }
}
