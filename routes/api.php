<?php

use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\RechargeController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\TelegramController;
use App\Http\Controllers\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->middleware('auth:sanctum')->group(function () {
    Route::get('users', [AuthenticateController::class, 'getUserInformation']);
});

Route::post('login', [AuthenticateController::class, 'loginAction']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('settings/', [SettingController::class, 'getInitSettingValueAction']);
    Route::get('settings/banks', [SettingController::class, 'getAllBankAction']);


    Route::post('recharges', [RechargeController::class, 'createRechargeAction']);
    Route::get('recharges', [RechargeController::class, 'getAllRechargeAction']);
    Route::get('recharges/{id}/preview', [RechargeController::class, 'previewRechargeAction']);

    Route::get('transactions', [TransactionController::class, 'getAllTransactionAction']);
    Route::post('transactions', [TransactionController::class, 'createTransactionAction']);

    Route::post('/telegram-webhook', [TelegramController::class, 'handleWebhook']);
});


